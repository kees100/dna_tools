<?php

abstract class Sequence {
    private $sequence_string;
    private $id;
    private $validate;
    
    public function __construct($sequence_string, $id, $validate) {
        $this->sequence_string = $sequence_string;
    }
    public function getSequenceString(){
        return $this->$sequence_string;

    }
    public function setSequenceString(){
        $this->$sequence_string = $sequence_string;

    }
    public function getId(){
        return $this->$id;

    }
    public function setId(){
        $this->$id = $id;

    }
    public function getId(){
        return $this->$id;

    }
    public function setId(){
        $this->$id = $id;

    }
    public function getValidate(){
        return $this-> $validate;

    }
    public function setValidate(){
        $this-> $validate = $validate;

    }
    public function __toString() {
        return sprintf("ID: %s. Sequence: %s", $this->id, $this->sequence_string);
    }
    
}