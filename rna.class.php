<?php
require_once "sequence.class.php";

class Rna extends Sequence {
    public function __construct($sequence_string, $id, $validate) {
        parent::__construct($sequence_string, $id, $validate);
    }
    public function __toString() {
        return sprintf("ID: %s. Sequence: %s", $this->id, $this->sequence_string);
    }
    
}